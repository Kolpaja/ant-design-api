import React, { useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./pages/Home";
import Cart from "./components/Cart"

import Collection from './components/Categories/Collection';
import NavLinks from './components/NavLinks';
import Login from './components/Login';
import Signup from './components/Login/Signup'
import User from './components/Users/User';
import ProductOverview from "./components/Products/ProductOverview"
import Footer from "./components/Footer"

import { Layout } from 'antd';
import About from "./pages/About"
import Shop from './pages/Shop'
import UserProfile from './components/Profile/UserProfile';
import ProfileSettings from './components/Profile/ProfileSettings';
import WishList from './components/WishList'
import PurchaseHistory from './components/Profile/PurchaseHistory';
const { Header } = Layout;


function App() {

  return (
    <Router>
      <Layout className="App">
        <Header className="Header">
          <NavLinks />
        </Header>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/cart" component={Cart} />
          <Route exact path="/shop" component={Shop} />
          <Route exact path="/user" component={UserProfile} />
          <Route exact path="/user/:id/settings" component={ProfileSettings} />
          <Route exact path="/user/:id/wishlist" component={WishList} />
          <Route exact path="/user/:id/history" component={PurchaseHistory} />
          <Route exact path="/users/login" component={Login} />
          <Route exact path="/users/signup" component={Signup} />
          <Route exact path="/users/:id" component={User} />
          <Route exact path="/products/:id" component={ProductOverview} />
          <Route exact path="/category/:params" component={Collection} />
        </Switch>
        <Footer />
      </Layout>
    </Router>
  );
}

export default App;
