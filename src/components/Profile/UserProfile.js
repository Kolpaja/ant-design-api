import React, { useState, useEffect } from 'react'
import "./styles.css";
import { Link } from 'react-router-dom';
import Map from "../MapBox/Map"
import { UserOutlined, SettingOutlined } from "@ant-design/icons";
import { capitalFirstLetter } from "../../util"

const UserProfile = ({ location }) => {
  // console.log("userprofile props", location.state)
  const user = location.state.user;
  console.log(user)

  const mapUser = { ...user, city: 'tirana', country: "albania" }
  // console.log("asdasd", mapUser)




  return (
    <div className="myprofile">
      <div className="myprofile-header">
        <span>My Profile</span>
      </div>
      <div className="myprofile-card">
        <div className="myprofile-card-left" >
          <div className="myprofile-user">
            <UserOutlined className="myprofile-userIcon" />
            <div className="myprofile-name">
              <span>Name: {capitalFirstLetter(user.name.firstname)}</span>
              <span>Surname: {capitalFirstLetter(user.name.lastname)}</span>
              <span>NickName: {user.username}</span>
            </div>
            <div className="myprofile-settings">
              <Link
                to={{
                  pathname: `/user/${user.id}/settings`,
                  state: { user: mapUser }
                }}
              ><SettingOutlined className="star settings" />Settings</Link>
            </div>
          </div>
        </div>
        <div className="myprofile-card-center" >
          <span className="myprofileAddress">Address</span>
          <span>City: {capitalFirstLetter(mapUser.city)}</span>
          <span>street: {user.address.street},nr <span>{user.address.number}</span></span>

          <span>Country: {capitalFirstLetter(mapUser.country)}</span>
          <div className="myprofile-mapbox">
            <Map user={mapUser} />
          </div>

        </div>
        <div className="myprofile-card-right" >
          <Link
            to={{
              pathname: `/user/${user.id}/history`,
              state: { id: user.id }
            }}>

            <div className="myprofile-purchase">
              <span className="history">Purchase History</span>
              <i className="fas fa-file-invoice"></i>
            </div>
          </Link>
          <Link
            to={{
              pathname: `/user/${user.id}/wishlist`,
              state: { id: user.id }
            }}>
            <div className="myprofile-wishlist">
              <span>Wish List</span>
              <i className="fas fa-grin-stars"></i>
            </div>
          </Link>
        </div>
      </div>
    </div >
  )
}

export default UserProfile

