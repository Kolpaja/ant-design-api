import React from 'react';
import "./styles.css"
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {
  Form, Input, Select, Checkbox, Button, message,
} from 'antd';

const ProfileSettings = ({ location }) => {
  const dispatch = useDispatch()
  const history = useHistory();

  const [form] = Form.useForm();
  const user = location.state.user
  console.log("setting user", user)

  const onFinish = async (values) => {
    console.log('Received values of form: ', values);
    try {
      // dispatch(addNewUser(values))
      message.success("Successfully updated your profile", [1.5], history.push("/"))
    } catch (error) {
      console.log(error)
    }
  };
  const userData = {
    name: {
      fisrtname: "sokol"
    }
  }

  return (
    <div className="settings-page">
      <div className="setting-header"><span>Update your profile</span></div>
      <div className="signup">
        <Form
          initialValues={userData}
          className="signup_form"
          form={form}
          name="register"
          onFinish={onFinish}
          scrollToFirstError
        >
          <Form.Item label="First Name" >
            <Form.Item
              initialValue={user.name.firstname}
              name={['name', 'firstname']}
              noStyle
              rules={[{ required: false, message: 'First name is required' }]}
            >
              <Input />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Last Name" >
            <Form.Item
              initialValue={user.name.lastname}
              name={['name', 'lastname']}
              noStyle
              rules={[{ required: false, message: 'Last Name is required' }]}
            >
              <Input />
            </Form.Item>
          </Form.Item>
          <Form.Item
            initialValue={user.username}
            name="username"
            label="Username"
            tooltip="What do you want the name to log in?"
            rules={[
              {
                required: false,
                message: 'Please input your username!',
                whitespace: false,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={user.email}
            name="email"
            label="E-mail"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: false,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            initialValue={user.password}
            name="password"
            label="Password"
            rules={[
              {
                required: false,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password placeholder="enter your password" />
          </Form.Item>

          <Form.Item
            initialValue={user.password}
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: false,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            initialValue={user.phone}
            name="phone"
            label="Phone Number"
            rules={[
              {
                required: false,
                message: 'Please input your phone number!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            initialValue={user.gender}
            name="gender"
            label="Gender"
            rules={[
              {
                required: false,
                message: 'Please select gender!',
              },
            ]}
          >
            <Select placeholder="select your gender">
              <Select.Option value="male">Male</Select.Option>
              <Select.Option value="female">Female</Select.Option>
              <Select.Option value="other">Other</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={user.city}
            name="city"
            label="City"
            rules={[
              {
                required: false,
                message: 'Please input your city',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={user.country}
            name="country"
            label="Country"
            rules={[{
              required: false,
              message: 'Please input your country',
            }]}>
            <Input />
          </Form.Item>

          <Form.Item label="Address">

            <Input.Group compact>
              <Form.Item
                initialValue={user.address.street}
                name={['address', 'street']}
                noStyle
              >
                <Input style={{ width: '50%' }} />
              </Form.Item>
              <Form.Item initialValue={user.address.number} name={['address', 'number']} noStyle>
                <Input style={{ width: '50%' }} />
              </Form.Item>
            </Input.Group>
          </Form.Item>

          <Form.Item
            initialValue={user.seller}
            name="seller"
            valuePropName="checked"
          >
            <Checkbox defaultChecked>
              Are you a seller?
            </Checkbox>

          </Form.Item>

          <div className="form-footer">
            <Form.Item  >
              <Button type="primary" htmlType="submit">
                Update profile
              </Button>
            </Form.Item>
          </div>
        </Form >
      </div>
    </div>
  )
}

export default ProfileSettings
