import React from 'react'
import { Link } from 'react-router-dom';
import "./styles.css";
import { useDispatch } from 'react-redux';

import { CaretUpOutlined, StarFilled, SettingOutlined } from '@ant-design/icons';
import { capitalFirstLetter } from "../../util"
import { toggleHidden, logoutUser } from "../../store/actions/singleUser"
import { message } from 'antd';


const Profile = ({ user }) => {
  const dispatch = useDispatch()
  // console.log(user)
  const handleSignout = () => {
    dispatch(logoutUser())
    message.success("Successefully logged out! See you soon", [1])
  }
  return (
    <div className="profile-popup">
      <CaretUpOutlined className="arrow-up-userinfo" />
      <div className="userinfo-name">
        <div>{capitalFirstLetter(user.name.firstname)} {capitalFirstLetter(user.name.lastname)}</div>
      </div>
      <div className="userinfo-profile">
        <Link
          to={{
            pathname: '/user',
            state: { user }
          }}
          onClick={() => dispatch(toggleHidden())}
        >My Profile</Link>
      </div>
      <div className="userinfo-wishlist">
        <Link
          to={{
            pathname: `/user/${user.id}/wishlist`,
            state: { id: user.id }
          }}
          onClick={() => dispatch(toggleHidden())}
        >My wishlist</Link><StarFilled className="star" style={{ fontSize: 20, color: "rgba(248, 184, 9, 0.959)" }} />
      </div>
      <div className="userinfo-settings">
        <Link
          to={{
            pathname: `/user/${user.id}/settings`,
            state: { user }
          }}
          onClick={() => dispatch(toggleHidden())}
        >Settings<SettingOutlined className="star settings" /></Link>
      </div>
      <div className="userinfo-btn">
        <span onClick={handleSignout}>Sign Out</span>
      </div>

    </div >
  )
}

export default Profile
