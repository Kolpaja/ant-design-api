import React, { useState, useEffect } from 'react';
import "./styles.css"
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { updateUser } from '../../store/actions/singleUser';
import Terms from "../../util/Terms"

import {
  Form, Input, Modal, Select, Checkbox, Button, message,
} from 'antd';

function Signup() {
  const dispatch = useDispatch()
  const history = useHistory();

  const [form] = Form.useForm();

  const fakeuser = {
    email: 'John@gmail.com',
    username: 'johnd',
    password: 'm38rmF$',
    name: {
      firstname: 'John',
      lastname: 'Doe'
    },
    phone: "12312312"
  }

  useEffect(() => {
  }, [])

  const onFinish = async (values) => {
    console.log('Received values of form: ', values);
    try {
      // dispatch(addNewUser(values))
      message.success("Successfully updated", [1.5], history.push('/'))
    } catch (error) {
      console.log(error)
    }


  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <div className="signup">
      <Form
        className="signup_form"
        form={form}
        name="register"
        onFinish={onFinish}
        scrollToFirstError
      >
        <Form.Item label="Name" >
          <Input.Group compact>
            <Form.Item
              name={['name', 'firstname']}
              noStyle
              rules={[{ required: false, message: 'First name is required' }]}
            >
              <Input style={{ width: '50%' }} placeholder="Enter First Name" />
            </Form.Item>
            <Form.Item
              name={['name', 'lastname']}

              noStyle
              rules={[{ required: false, message: 'Last Name is required' }]}
            >
              <Input style={{ width: '50%' }} placeholder="Enter Last Name" />
            </Form.Item>
          </Input.Group>
        </Form.Item>

        <Form.Item
          name="username"
          label="Username"
          tooltip="What do you want the login name to be?"
          rules={[
            {
              required: false,
              message: 'Please input your username!',
              whitespace: false,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: false,
              message: 'Please input your E-mail!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Password"
          rules={[
            {
              required: false,
              message: 'Please input your password!',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: false,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error('The two passwords that you entered do not match!'));
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>


        <Form.Item
          name="phone"
          label="Phone Number"
          rules={[
            {
              required: false,
              message: 'Please input your phone number!',
            },
          ]}
        >
          <Input

            style={{
              width: '100%',
            }}
          />
        </Form.Item>

        <Form.Item
          name="gender"
          label="Gender"
          rules={[
            {
              required: false,
              message: 'Please select gender!',
            },
          ]}
        >
          <Select placeholder="select your gender">
            <Select.Option value="male">Male</Select.Option>
            <Select.Option value="female">Female</Select.Option>
            <Select.Option value="other">Other</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Address">
          <Input.Group compact>
            <Form.Item
              name={['address', 'city']}
              noStyle
              rules={[{ required: false, message: 'Province is required' }]}
            >
              <Input style={{ width: '50%' }} placeholder="Input city" />
            </Form.Item>
            <Form.Item
              name={['address', 'street']}
              noStyle
              rules={[{ required: false, message: 'Street is required' }]}
            >
              <Input style={{ width: '50%' }} placeholder="Input street" />
            </Form.Item>
          </Input.Group>
        </Form.Item>

        <Form.Item
          name="seller"
          valuePropName="checked"
        >
          <Checkbox defaultChecked>
            Are you a seller?
          </Checkbox>

        </Form.Item>

        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
            },
          ]}

        >
          <Checkbox >
            I have read the  <a type="primary" onClick={showModal}>agreement</a>
          </Checkbox>
        </Form.Item>


        <Form.Item >
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form >

      <Modal
        title="Website Terms and Conditions"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"60%"}
        style={{ maxHeight: 500, overflowY: "auto" }}
      >
        <Terms />
      </Modal>
    </div>
  )
}


export default Signup
