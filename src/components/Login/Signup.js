import React, { useState, useEffect } from 'react';
import "./styles.css"
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addNewUser } from '../../store/actions/singleUser';
import Terms from "../../util/Terms"
import axios from 'axios'

import {
  Form, Input, Modal, Select, Checkbox, Button, message,
} from 'antd';

function Signup() {
  const dispatch = useDispatch()
  const history = useHistory();

  const [form] = Form.useForm();

  const fakeuser = {
    email: 'John@gmail.com',
    username: 'johnd',
    password: 'm38rmF$',
    name: {
      firstname: 'John',
      lastname: 'Doe'
    },
    phone: "12312312"
  }

  useEffect(() => {
    dispatch(addNewUser(fakeuser))
  }, [])

  const onFinish = async (values) => {
    console.log('Received values of form: ', values);
    try {
      // dispatch(addNewUser(values))
      message.success("Successfully added new user", [1.5], history.push('/'))
    } catch (error) {
      console.log(error)
    }


  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <div className="signup-header"><span>Sign Up Form</span></div>
      <div className="signup">
        <Form
          className="signup_form"
          form={form}
          name="register"
          onFinish={onFinish}
          scrollToFirstError
        >
          <Form.Item label="Last Name" >
            <Form.Item
              name={['name', 'firstname']}
              noStyle
              rules={[{ required: false, message: 'First name is required' }]}
            >
              <Input placeholder="Enter First Name" />
            </Form.Item>
          </Form.Item>
          <Form.Item label="First Name" >
            <Form.Item
              name={['name', 'lastname']}
              noStyle
              rules={[{ required: false, message: 'Last Name is required' }]}
            >
              <Input placeholder="Enter Last Name" />
            </Form.Item>
          </Form.Item>
          <Form.Item
            name="username"
            label="Username"
            tooltip="What do you want the name to log in?"
            rules={[
              {
                required: false,
                message: 'Please input your username!',
                whitespace: false,
              },
            ]}
          >
            <Input placeholder="Enter your username" />
          </Form.Item>
          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: false,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input placeholder="name@mail.com" />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: false,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password placeholder="enter your password" />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: false,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <Input.Password placeholder="re-enter your password" />
          </Form.Item>


          <Form.Item
            name="phone"
            label="Phone Number"
            rules={[
              {
                required: false,
                message: 'Please input your phone number!',
              },
            ]}
          >
            <Input placeholder="1-570-236-7033" />
          </Form.Item>

          <Form.Item
            name="gender"
            label="Gender"
            rules={[
              {
                required: false,
                message: 'Please select gender!',
              },
            ]}
          >
            <Select placeholder="select your gender">
              <Select.Option value="male">Male</Select.Option>
              <Select.Option value="female">Female</Select.Option>
              <Select.Option value="other">Other</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="city"
            label="City"
            rules={[
              {
                required: false,
                message: 'Please input your city',
              },
            ]}
          >
            <Input placeholder="Input your city" />
          </Form.Item>
          <Form.Item
            name="country"
            label="Country"
            rules={[{
              required: false,
              message: 'Please input your country',
            }]}>
            <Input placeholder="Input yor country" />
          </Form.Item>

          <Form.Item label="Address">
            <Input.Group compact>
              <Form.Item
                name={['address', 'street']}
                noStyle
              >
                <Input style={{ width: '50%' }} placeholder="Input street" />
              </Form.Item>
              <Form.Item name={['address', 'number']} noStyle>
                <Input style={{ width: '50%' }} placeholder="Input number" />
              </Form.Item>
            </Input.Group>
          </Form.Item>

          <Form.Item
            name="seller"
            valuePropName="checked"
          >
            <Checkbox defaultChecked>
              Are you a seller?
            </Checkbox>

          </Form.Item>

          <Form.Item
            name="agreement"
            valuePropName="checked"
            rules={[
              {
                validator: (_, value) =>
                  value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
              },
            ]}

          >
            <Checkbox >
              I have read the  <a type="primary" onClick={showModal}>agreement</a>
            </Checkbox>
          </Form.Item>


          <Form.Item >
            <Button type="primary" htmlType="submit">
              Register
            </Button>
          </Form.Item>
        </Form >

        <Modal
          title="Website Terms and Conditions"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          width={"60%"}
          style={{ maxHeight: 500, overflowY: "auto" }}
        >
          <Terms />
        </Modal>
      </div>
    </>
  )
}


export default Signup
