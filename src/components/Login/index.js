import React, { useState, useEffect } from 'react';
import "./styles.css";
import * as api from "../../api"

import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from "react-redux";

import { Form, Checkbox, Button, Input, Typography, message } from "antd";


function Login() {
  const dispatch = useDispatch();
  const history = useHistory();

  //? username: "kevinryan"
  //? password: "kev02937@"
  const onFinish = async (values) => {
    try {
      console.log(values)
      const res = await api.authLogin(values);
      console.log("login res:", res.data.user)
      res.data.status == "Error" ?
        message.error(`${res.data.msg}`, [2], `${history.push("/users/login")}`) :
        dispatch({ type: "AUTH", data: res.data?.user }) && message.success("You successfully logged in!", [1.5], `${history.push('/')}`)
    } catch (error) {
      console.log("login err:", error)
    }
  };


  useEffect(() => {

  }, [history])

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="login">

      <Typography.Title className="login-title" type="secondary" level={3}><span>Login</span></Typography.Title>
      <Form
        className="login-form"
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please enter your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please enter your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        {/* <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item> */}

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button> or <Link type="primary" to='/users/signup' >Register Now!</Link>
        </Form.Item>
      </Form>
    </div >
  )
}

export default Login
