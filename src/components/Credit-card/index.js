import React from "react";
import "./styles.css";
import visa from "../../assets/credit-card/visa.jpeg"
import chip from "../../assets/credit-card/chip.jpg"

const CreditCard = () => {

  return (
    <div className="card">
      <div className="base">
        <span className="credit">Credit Card</span>
        <span className="bank-name"><img src={visa} /></span>
        <span className="chip"><img src={chip} /></span>
        <span className="nr">4242  4242  4242  4242</span>
        <span className="cvv"> CVV: 123</span>
        <span className="card-dtl">Valid: 11/22</span>
        <div className="name">Mr. Test Card</div>
      </div>
    </div >
  )
}

export default CreditCard;