import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom"

import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "../../store/actions/users"
// import { getAllCartsItems } from "../../store/actions/cartsAll"

import { Row, Spin, Avatar, Tooltip } from 'antd';
import { capitalFirstLetter } from '../../util'

import { PlusCircleOutlined } from '@ant-design/icons';


function Users() {
  const dispatch = useDispatch();
  const users = useSelector(state => state.users)
  // const carts = useSelector(state => state.carts)

  // console.log("users comp: ", users)
  useEffect(() => {
    dispatch(getAllUsers());
    // dispatch(getAllCartsItems());
  }, [dispatch])

  return (
    <Row gutter={[12, 48]} justify="space-around">
      {users.length > 0 ? users.map(user => {
        const randomColor = Math.floor(Math.random() * 16777215).toString(16);
        const randomColor1 = Math.floor(Math.random() * 16777215).toString(16);
        return <div key={user.id}>
          <Tooltip
            title={`${capitalFirstLetter(user.name.firstname)} ${capitalFirstLetter(user.name.lastname)}`}
            color={`#${randomColor1}`} >
            <Link
              to={{
                pathname: `/users/${user.id}`,
                state: { user },
              }}
            ><Avatar

              style={{
                backgroundColor: `#${randomColor}`,
                verticalAlign: 'middle',
              }}
              size="large"
            >
                {capitalFirstLetter(user.name.firstname)}
              </Avatar></Link>
          </Tooltip>
        </div>
      }) :
        <div style={{ position: 'relative', width: "100vw", height: "100vh" }}>
          <Spin size="large" style={{ position: 'absolute', top: "50%", left: "50%" }} />
        </div>
      }

    </Row>
  )
}

export default Users
