import React from 'react'
import './styles.css'
import { Link } from 'react-router-dom';
import { Button, Descriptions } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { capitalFirstLetter } from "../../util"

const User = ({ location }) => {
  // console.log(location)
  const { user } = location.state
  console.log(user)
  return (
    <div className="User">
      <h1>User Info</h1>
      <Descriptions title={`${capitalFirstLetter(user.name.firstname)} ${capitalFirstLetter(user.name.lastname)}`}>
        <Descriptions.Item label="Username">{user.username}</Descriptions.Item>
        <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
        <Descriptions.Item label="Live">{capitalFirstLetter(user.address.city)}</Descriptions.Item>
        <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
        <Descriptions.Item label="Address">
          {` No. ${user.address.number}, ${user.address.street},${capitalFirstLetter(user.address.city)}`}
        </Descriptions.Item>
      </Descriptions>
      <Link to="/users/signup"><Button type="primary">Register!</Button></Link>
    </div>
  )
}

export default User
