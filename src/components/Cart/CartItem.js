import React from 'react'
import "./styles.css";


import { useDispatch, useSelector } from 'react-redux'

import { Tooltip } from 'antd';
import { CaretUpOutlined, CaretDownOutlined, CloseOutlined } from '@ant-design/icons'

// import moment from 'moment';

const CartItem = ({ item, addItem, removeItem, removeOneItem }) => {
  // const state = useSelector(state => state.state)
  // console.log("item added to cart: ", item)





  return (
    <div className="cart-item">
      <img src={item.image} style={{ width: 40 }} />
      <span>{item.title.substring(0, 15)}</span>

      <Tooltip title="Add one more" ><span className="left-arrow" ><CaretUpOutlined onClick={() => addItem(item)} /></span></Tooltip>
      <span className="item-quantity">{item.quantity} X ${item.price}</span>
      <Tooltip title="Remove one!" ><span className="right-arrow"><CaretDownOutlined onClick={() => removeOneItem(item)} /></span></Tooltip>


      <Tooltip title="Remove Item" >
        <span className="item-remove">
          <CloseOutlined onClick={() => removeItem(item)} />
        </span>
      </Tooltip>
      {/* <span>{item.price * item.quantity} $</span> */}
    </div>
  )
}

export default CartItem
