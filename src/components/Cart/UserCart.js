import React from 'react';
import "./styles.css"
import { useSelector, useDispatch } from 'react-redux';
import { Button } from "antd"

import { addItem, removeOneItem, clearCart, purchaseCart, removeItem } from '../../store/actions/cartsAll'
import CartItem from './CartItem';
import StripeCheckoutButton from '../Stripe-Button';
import CreditCard from '../Credit-card';


const UserCart = ({ userId }) => {
  const dispatch = useDispatch()
  const carts = useSelector(state => state.carts)
  // console.log("carts: ", carts)

  let total = carts.cartItems.reduce((accumulatedQuantity, cartItem) => accumulatedQuantity + cartItem.quantity * cartItem.price, 0)
  const handleAddItem = (item) => dispatch(addItem(item));
  const handleRemoveOneItem = (item) => dispatch(removeOneItem(item));
  const handleRemoveItem = (item) => dispatch(removeItem(item));

  const handlePurchase = (userId) => {
    dispatch(purchaseCart(userId));
    // console.log("purchased: ", carts)
  }

  // console.log("cartItems:", cartItems)
  // console.log("userId, ", userId)
  return (
    <div className="users-cart">
      <div className="cart-header">
        <span>Item</span>
        <span>Name</span>
        <span>Quantity</span>
        <span>Remove</span>
      </div>
      {carts.cartItems.length > 0 ? (carts.cartItems.map(
        item => <CartItem addItem={handleAddItem} removeOneItem={handleRemoveOneItem} removeItem={handleRemoveItem} key={item.id} item={item} />
      )) : "Your Cart is empty!"
      }
      {/* <Button onClick={handlePurchase}>Purchase</Button> */}
      {/* <Button onClick={() => dispatch(clearCart())}>Clear</Button> */}
      {carts.cartItems.length > 0 && <>
        <span span className="total">Total: {total.toFixed(2)} $</span>
        <div className="pymt-dtls">
          <div className="pay-now">
            <StripeCheckoutButton price={total} />
          </div>
          <div className="visa-card">
            *Please use the following test credit card*
            <CreditCard />
          </div>
        </div>

      </>}
    </div >
  )
}

export default UserCart
