import React from 'react';
import { Link } from "react-router-dom"
import Collection from "./Collection"
import "./styles.css"

import { Card, Image, Row, Col, Divider } from "antd";

import electronicsImg from "../../assets/category-img/electronics.jpg"
import jeweleryImg from "../../assets/category-img/jewelery.jpg"
import menClothingImg from "../../assets/category-img/men's clothing.jpg"
import womenClothingImg from "../../assets/category-img/Womens-Clothes.jpg"

function Categories() {
  const categories = ["electronics", "jewelery", "men's clothing", "women's clothing"];

  return (
    <Row justify="space-around">
      <Divider plain orientation="center">Explore our wonderful collections</Divider>
      <Card
        className="collecion-card"
        hoverable
        cover={<img style={{ height: 180, borderRadius: 10, padding: 5 }} alt="example" src={electronicsImg} />}
        style={{ width: 240, borderRadius: 10 }}
        actions={[<Link
          to={{
            pathname: `/category/${categories[0]}`,
            state: { category: categories[0] },
          }}
        >Visit Collection</Link>]}
      >
        <Card.Meta title="Electronics" align="center" />
      </Card>

      <Card
        className="collecion-card"
        hoverable
        style={{ width: 240, borderRadius: 10 }}
        cover={<img style={{ height: 180, borderRadius: 10, padding: 10 }} alt="example" src={jeweleryImg} />}
        actions={[<Link
          to={{
            pathname: `/category/${categories[1]}`,
            state: { category: categories[1] },
          }}
        >Visit Collection</Link>]}
      >
        <Card.Meta title="Jewelery" align="center" />
      </Card>

      <Card
        className="collecion-card"
        hoverable
        style={{ width: 240, borderRadius: 10 }}
        cover={<img style={{ height: 180, borderRadius: 10, padding: 10 }} alt="example" src={menClothingImg} />}
        actions={[<Link
          to={{
            pathname: `/category/${categories[2]}`,
            state: { category: categories[2] },
          }}
        >Visit Collection</Link>]}
      >
        <Card.Meta title="Men's clothing" align="center" />
      </Card>

      <Card
        className="collecion-card"
        hoverable
        style={{ width: 240, borderRadius: 10 }}
        cover={<img style={{ height: 180, borderRadius: 10, padding: 10 }} alt="example" src={womenClothingImg} />}
        actions={[<Link
          to={{
            pathname: `/category/${categories[3]}`,
            state: { category: categories[3] },
          }}
        >Visit Collection</Link>]}
      >
        <Card.Meta title="Women's clothing" align="center" />
      </Card>
    </Row >
  )
}

export default Categories
