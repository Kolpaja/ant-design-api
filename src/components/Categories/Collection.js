import React, { useState, useEffect } from 'react';
import { Image, Spin } from "antd"

import { useDispatch } from 'react-redux';
import { getCollection } from '../../store/actions/collections';
import { useSelector } from "react-redux"
import ProductItem from '../Products/ProductItem';


function Collection({ location }) {

  const { category } = location.state

  const dispatch = useDispatch()

  const collection = useSelector(state => state.collections);
  const user = useSelector(state => state.user)

  useEffect(() => {
    dispatch(getCollection(category))
  }, [])
  return (
    <>
      {collection.length > 0 ? (collection.map(item => <ProductItem item={item} user={user} />)) : <div style={{ position: 'relative', width: "100vw", height: "100vh" }}>
        <Spin size="large" style={{ position: 'absolute', top: "50%", left: "50%" }} />
      </div>}
    </>
  )
}

export default Collection
