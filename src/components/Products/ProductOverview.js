import React from 'react'
import ProductItem from './ProductItem'

const ProductOverview = ({ location }) => {
  // console.log(props)
  const { item, user } = location.state;
  return (
    <>
      <ProductItem item={item} user={user} />
    </>
  )
}

export default ProductOverview