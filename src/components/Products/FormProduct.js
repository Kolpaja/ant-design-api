import React from 'react'
import {
  Button, Form, Input, Select, message
} from 'antd';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { updateCurrentProduct } from "../../store/actions/products";

const FormProduct = ({ item, handleCancel, handleCancelUpdate, handleAddProduct }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  // console.log("item: ", item)

  const updateProductFnc = async (id, values) => {
    try {
      const res = await dispatch(updateCurrentProduct(id, values));
      // console.log("handle add new: ", res)
      message.success('New product successfully updated!', [1.5], history.push('/'));

    } catch (error) {
      console.log(error)
    }
  }
  const onFinish = (values) => {
    // console.log(values);
    item ?
      (updateProductFnc(item.id, values) && handleCancelUpdate()) :
      (handleAddProduct(values) && handleCancel());
  }

  return (
    <div>
      <Form
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        size="large"
        onFinish={onFinish}
        initialValues={item}

      >
        <Form.Item
          label="Title"
          name="title"

          rules={[{
            required: true,
            message: "Please enter the product title "
          }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Price" name="price" rules={[{ required: false, }]}>
          <Input type="number" step="0.01" min={1} max={999} />
        </Form.Item>
        <Form.Item
          label="Description"
          rules={[{ required: true, }]}
          name="description">
          < Input type="text" />
        </Form.Item>
        <Form.Item
          label="Image"
          name="image"
          rules={[{
            required: false,
            message: "Please enter the image url"
          }]}
        >
          <Input type="url" />
        </Form.Item>
        <Form.Item label="Category" name="category" rules={[{ required: true, message: "Please select a category!" }]}>
          <Select>
            <Select.Option value="electronic">electronic</Select.Option>
            <Select.Option value="jewelery">Jewelery</Select.Option>
            <Select.Option value="women's clothing">Women's clothing</Select.Option>
            <Select.Option value="men's clothing">Men's clothing</Select.Option>
          </Select>
        </Form.Item>

        <Button type="ghost" block="true" htmlType="submit">
          Submit
        </Button>

      </Form>
    </div>
  )
}

export default FormProduct
