import React, { useState, useEffect } from 'react';
import './styles.css'
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from 'react-router';

import { deleteOneProduct, createProduct } from "../../store/actions/products"
import { addItem, addUserToCart } from "../../store/actions/cartsAll";
import { addToWishlist } from "../../store/actions/wishlist"

import {
  Card, Row, Col, Rate,
  Spin, message, Tooltip, Popconfirm, Modal
} from 'antd';

import { HeartOutlined, HeartFilled, EditOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import FormProduct from './FormProduct';


function Products({ products, userId }) {
  // const [wished, setWished] = useState(false);
  // console.log(wished)

  const dispatch = useDispatch();
  const history = useHistory();
  const search = useSelector(state => state.products.search)
  const user = userId;

  const handleAddItem = (item) => dispatch(addItem(item));

  const handleClickAdd = (prop) => message.info(`Please Login to ${prop} item`);

  const wishlist = useSelector(state => state.wishlist.wishlist)

  //delete one product
  const handleDeleteOneProduct = async (id) => {
    await dispatch(deleteOneProduct(id))
    message.success('Item successfully deleted!');
  }


  const handleAddProduct = async (values) => {
    try {
      const res = await dispatch(createProduct(values));
      console.log("handle add new: ", res)
      message.success('New product successfully added!', [1.5], history.push('/'));

    } catch (error) {
      console.log(error)
    }
  }

  //*form set up
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);
  const [updateProductItem, setUpdateProductItem] = useState({});

  //* update product
  const showModal = () => {
    setIsModalVisible(true);
  };


  const showModalUpdate = () => {
    setIsModalVisibleUpdate(true);
  };

  const handleCancelUpdate = () => {
    setIsModalVisibleUpdate(false);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  function cancel() {
    console.log("Cancel Delete");
  }
  //*edit 1 product

  const handleEditProduct = (item) => {
    // console.log("updating :", item.id);
    setUpdateProductItem(item)
    showModalUpdate();
  }

  // for (let i = 0; i < wishlist.length; i++) {
  //   if (wishlist[i].id === item.id) { return setWished(true) }
  //   else return dispatch(addToWishlist(item))
  // }
  // console.log(wishlist)

  const handleAddToWishItem = (item) => dispatch(addToWishlist(item))




  useEffect(() => {
    (user && dispatch(addUserToCart(userId.id)))
  }, [handleAddProduct, dispatch])
  return (
    <>
      {products.length > 0 ? (
        <Row gutter={[12, 48]} justify="space-around">
          {products.filter(product => product.title.toLowerCase().includes(search.trim().toLowerCase())).map(item => {
            // console.log("item: ", item)
            return (
              <Col key={item.id}   >
                <Card
                  hoverable
                  style={{ width: 230, borderRadius: 10 }}
                  cover={
                    <Link
                      to={{
                        pathname: `/products/${item.id}`,
                        state: { item, user },
                      }}
                    >
                      <img style={{ margin: 10, width: 210, height: 180 }} alt={item.title} src={item.image} />
                    </Link>}
                  actions={
                    [
                      user ? <Tooltip title="Add Item"><PlusOutlined onClick={() => handleAddItem(item)} /> </Tooltip>
                        : <Tooltip title="Add Item"><PlusOutlined style={{ fontSize: 20 }} onClick={() => handleClickAdd("Add")} /></Tooltip>,

                      user ? <Tooltip title="Add Wish"><HeartFilled style={{ fontSize: 20 }} onClick={() => handleAddToWishItem(item)} /> </Tooltip>
                        : <Tooltip title="Add Wish"><HeartFilled style={{ fontSize: 20 }} onClick={() => handleClickAdd("Wish")} /></Tooltip>,
                      user ? <Tooltip title="Edit"><EditOutlined key="edit" onClick={() => handleEditProduct(item)} /></Tooltip>
                        : <Tooltip title="Edit"><EditOutlined onClick={() => handleClickAdd("Edit")} /></Tooltip>,
                      user ? <Popconfirm
                        title="Are you sure to delete this item?"
                        onConfirm={() => handleDeleteOneProduct(item.id)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                      ><Tooltip title="Delete" placement="bottom"><DeleteOutlined /> </Tooltip>
                      </Popconfirm>
                        : <Tooltip title="Delete"><DeleteOutlined onClick={() => handleClickAdd("Delete")} /></Tooltip>,
                    ]}
                >
                  <Rate align="middle" character={<HeartOutlined />} allowHalf style={{ color: 'red', fontSize: 20 }} defaultValue={item.rating.rate} />
                  <Card.Meta title={item.title} align="start" />

                </Card>

              </Col>

            )
          }
          )}
          <Col style={{ position: 'relative' }}>
            <Card
              className="add_product_card"
              hoverable
              style={{ width: 240, height: 325, position: 'relative' }}
            >
              <Tooltip
                onClick={showModal}
                title="Add New Product"
              >
                <PlusOutlined className="add_product_plus" />
              </Tooltip>
              <Modal
                title="Add A New Product"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={null}
                destroyOnClose
              >
                <FormProduct handleCancel={handleCancel} handleAddProduct={handleAddProduct} />
              </Modal>
              <Modal
                title="Edit & Update A Product"
                visible={isModalVisibleUpdate}
                onCancel={handleCancelUpdate}
                footer={null}
                destroyOnClose
              >
                <FormProduct handleCancelUpdate={handleCancelUpdate} handleEditProduct={handleEditProduct} item={updateProductItem} />
              </Modal>
            </Card>
          </Col>
        </Row>
      ) :
        <div style={{ position: 'relative', width: "100vw", height: "100vh" }}>
          <Spin size="large" style={{ position: 'absolute', top: "50%", left: "40%", transform: "translate(-50%)" }} />
        </div>
      }
    </>
  )
}

export default Products
