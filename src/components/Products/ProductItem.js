import React from 'react';
import "./styles.css"
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { capitalFirstLetter } from "../../util"

import { addItem } from '../../store/actions/cartsAll';
import { deleteOneProduct } from '../../store/actions/products';
import { addToWishlist } from "../../store/actions/wishlist"

import { HeartOutlined, HeartFilled } from "@ant-design/icons"
import { Image, Button, Typography, Divider, Spin, Space, Rate, message, Tooltip } from 'antd';
const { Title, Paragraph } = Typography


const ProductItem = ({ user, item }) => {
  // console.log("product item", item)
  const dispatch = useDispatch();
  const history = useHistory();

  const handleAddToCart = (item) => dispatch(addItem(item));
  const handleAddToWishItem = (item) => {
    dispatch(addToWishlist(item))
  }
  const handleDelete = async (item) => {
    await dispatch(deleteOneProduct(item.id))
    message.success("Product successfullu delete", [1], history.push("/"))
  }

  return (
    <div className="product-item">
      <Title level={2}>{item.title}</Title>
      <Divider />
      <Space align="start">
        <Space className="image" direction="vertical">
          <Image
            className="item-image"
            alt={`${item.title}`}
            src={item.image}
          />
          <div className="rating" align="center">
            Rate: <Rate className="rate" align="middle" character={<HeartOutlined />} allowHalf defaultValue={item.rating.rate} />
          </div>
        </Space>
        <Space direction="vertical" className="item-details" >
          <Paragraph>
            <Title level={3}>Description</Title>
            {item.description}
          </Paragraph>
          <Title level={4}>Category: {capitalFirstLetter(item.category)}</Title>

          <span>Price: ${item.price}</span>
          <Space>
            <Button type="primary" onClick={() => handleAddToCart(item)}>Add to cart</Button>
            <Tooltip title="Add Wish" placement="right" ><HeartFilled style={{ marginLeft: 20, fontSize: 30, color: " red" }} onClick={() => handleAddToWishItem(item)} /> </Tooltip>
            {/* {sellerItem.userId === user.id && <><Button >Edit</Button> <Button onClick={handleDelete} >Delete</Button></>} */}
          </Space>
        </Space>
      </Space>

    </div >
  )
}

export default ProductItem
