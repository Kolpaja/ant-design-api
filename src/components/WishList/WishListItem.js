import React from 'react'
import "./styles.css"
import { useDispatch } from 'react-redux';
import { Tooltip, message } from 'antd';
import { PlusSquareOutlined, DeleteOutlined, CloseOutlined } from '@ant-design/icons'
import { addItem } from "../../store/actions/cartsAll"
import { removeOneWish } from "../../store/actions/wishlist"

const WishListItem = ({ item }) => {
  const dispatch = useDispatch()
  console.log("WishListItem", item)
  const handleClickAdd = (item) => {
    dispatch(addItem(item));
    message.success("Successfully added to cart!", [1.5], dispatch(removeOneWish(item)))

  }

  const handleClickRemove = item => {
    dispatch(removeOneWish(item))
  }

  return (
    <div className="wishlistitem">
      <div className="wishlist-card">
        <img src={item.image} className="wishlist-img" />
        <span className="wishlist-title">{item.title}</span>
        <span > ${item.price}</span>
        <Tooltip title="Add to cart!"><PlusSquareOutlined onClick={() => handleClickAdd(item)} className="clickable-icon" style={{ fontSize: 30, color: "rgb(158, 18, 223)" }} /></Tooltip>
        <Tooltip title="Remove"><DeleteOutlined onClick={() => handleClickRemove(item)} className="clickable-icon" style={{ fontSize: 30, color: "rgb(18, 131, 223)" }} /></Tooltip>
      </div>
    </div>
  )
}

export default WishListItem
