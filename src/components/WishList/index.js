import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import WishListItem from "./WishListItem"
import { Spin, Tooltip } from "antd"
import { StarFilled } from "@ant-design/icons"
import sleeping from "../../assets/img/Man_Sleeping.png"

const WishList = ({ location }) => {
  console.log(location)
  const id = location.state.id

  console.log("userid: ", id)
  const dispatch = useDispatch()
  const { wishlist } = useSelector(state => state.wishlist)
  console.log("wishlist", wishlist)
  return (
    <div className="wishlist-page">
      <h1>
        <StarFilled className="star wishlist-star" style={{ fontSize: 20, color: "rgba(248, 184, 9, 0.959)" }} />
        My wish list
        <StarFilled className="star wishlist-star" style={{ fontSize: 20, color: "rgba(248, 184, 9, 0.959)" }} /></h1>
      {wishlist.length > 0 ?
        <>
          <div className="wishlist">
            <div className="wishlist-header">
              <span>Product</span>
              <span>Title</span>
              <span >Price</span>
              <span></span>
            </div>
            {wishlist.map(item => <WishListItem key={item.id} item={item} />)}
          </div>
        </>
        : <div className="wishlist-empty">
          <img src={sleeping} />
          <Tooltip title="Add your wished items here!">
            <Spin size="large" style={{ position: 'absolute', top: "80px", left: "49%" }} />
          </Tooltip>
        </div>}
    </div>
  )
}

export default WishList
