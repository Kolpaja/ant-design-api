import React from 'react'
import { useSelector } from 'react-redux'

const WishlistModal = () => {
  const { wishlist } = useSelector(state => state.wishlist)
  // console.log("open wish modal", wishlist)

  return (
    <div>
      {wishlist.length > 0 ? wishlist.map(item => {
        return <div key={item.id} className="wishlist-card" >
          <img src={item.image} className="wishlist-img" />
          <span className="wishlist-title">{item.title}</span>
        </div>
      })
        : "There are no wished items yet!"
      }

    </div>
  )
}

export default WishlistModal
