import React, { useState, useEffect } from 'react'
import ReactMapGL, { Marker, NavigationControl, AttributionControl } from 'react-map-gl';
import { getMap } from "./geocoding"

function Map({ user }) {
  // console.log(user)
  const mapboxToken = "pk.eyJ1Ijoia29scGFqYSIsImEiOiJja293eXhuNmYwYXowMndvNzI1ajllMTMyIn0.PmoaBxRXm36CfIqivdtUDw"
  const [lat, setLat] = useState(0)
  const [long, setLong] = useState(0)

  // () => setLat();
  // () => setLong();


  useEffect(async () => {
    const geometry = await getMap(user)
    await setLat(geometry[0])
    await setLong(geometry[1])
  }, [])
  // console.log(lat);
  const navControlStyle = {
    right: 10,
    top: 10
  };
  return (
    <ReactMapGL
      container='map'
      width={300}
      height={200}
      latitude={lat}
      longitude={long}
      zoom={8}
      mapboxApiAccessToken={mapboxToken}
      mapStyle='mapbox://styles/mapbox/streets-v11'

    >
      <Marker
        latitude={41.33}
        longitude={19.82}
        offsetLeft={-5}
        offsetTop={-29}>
        <i className="fas fa-map-marker-alt" style={{ color: "red", fontSize: 18 }}></i>
      </Marker>

    </ReactMapGL>
  );
}

export default Map