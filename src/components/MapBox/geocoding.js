import mbxGeocoding from '@mapbox/mapbox-sdk/services/geocoding';
const geocoder = mbxGeocoding({
  accessToken: "pk.eyJ1Ijoia29scGFqYSIsImEiOiJja293eXhuNmYwYXowMndvNzI1ajllMTMyIn0.PmoaBxRXm36CfIqivdtUDw"
})


export const getMap = async (address) => {
  const geodata = await geocoder.forwardGeocode({
    query: `${address.city}, ${address.country}`,
    limit: 1
  }).send()

  const reversed = geodata.body.features[0].geometry.coordinates.reverse()
  // console.log(reversed)
  return reversed
};