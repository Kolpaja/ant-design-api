import React from 'react'
import "./styles.css"
import { HeartFilled } from "@ant-design/icons"
import { Typography } from 'antd'

const Footer = () => {
  return (
    <div className="footer-div">
      <div className="footer-dtls">
        <div>
          Made with <span className="heart"><HeartFilled /></span> from <Typography.Link target="_blank" href="https://codevider.com/">CodeVider</Typography.Link>
        </div>
        <div className="copyright">&copy; <Typography.Link target="_blank" href="https://www.linkedin.com/in/sokol-paja/" >kolpaja</Typography.Link></div>

      </div>
    </div>
  )
}

export default Footer
