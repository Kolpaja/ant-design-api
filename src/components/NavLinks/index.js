import React, { useState, useEffect } from 'react';
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';

import "./styles.css";
import logoFake from "../../assets/Fakebay-logo.png"
import logoF from "../../assets/romb-f.png"

import Cart from '../Cart';
import UserCart from '../Cart/UserCart';
import Profile from '../Profile';
import WishlistModal from '../WishList/WishlistModal';
import { getOneUser, toggleHidden } from "../../store/actions/singleUser"
import { searchProducts } from "../../store/actions/products"
import { capitalFirstLetter } from "../../util";

import { ShoppingCartOutlined, EditOutlined, UserOutlined, CaretDownOutlined } from '@ant-design/icons'
import { Badge, Input, Modal, Button, Dropdown, Menu, message, Typography } from 'antd';
const { Search } = Input;
const { SubMenu } = Menu;




function NavLinks() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalWishlistVisible, setIsModalWishlistVisible] = useState(false);
  const [searchField, setSearchField] = useState('')
  const dispatch = useDispatch();
  const history = useHistory();

  const [userT, setUserT] = useState(JSON.parse(localStorage.getItem('token')));
  const user = useSelector(state => state.user.user);
  // console.log(user)
  const hidden = useSelector(state => state.user.hidden);
  const products = useSelector(state => state.products.products)
  // const search = useSelector(state => state.products.search)
  // console.log("search: ", search)
  const { cartItems } = useSelector(state => state.carts);

  let counter = cartItems.reduce((accumulatedQuantity, cartItem) => accumulatedQuantity + cartItem.quantity, 0)

  const showModal = () => { setIsModalVisible(true); };
  const handleOk = () => { setIsModalVisible(false); };
  const handleCancel = () => { setIsModalVisible(false); };

  const showModalWishlist = () => { setIsModalWishlistVisible(true); };
  const handleOkWishlist = () => { setIsModalWishlistVisible(false); };
  const handleCancelWishlist = () => { setIsModalWishlistVisible(false); };

  const handleSignOut = () => {
    handleSignOutToken();
    history.push('/')
  }
  const handleSignOutToken = () => {
    console.log("loging out...")
    dispatch({ type: "LOGOUT" })
    setUserT('')
    message.info("You successfully logged out!", [1.5], `${history.push('/')}`)
  }

  //? search products
  const onSearch = (value) => {
    dispatch(searchProducts(value))
  }
  const handleChange = e => { dispatch(searchProducts(e.target.value)) }


  //? password: "kev02937@"
  //? username: "kevinryan"
  useEffect(() => {
    setUserT(JSON.parse(localStorage.getItem('token')));
    dispatch(getOneUser(3))
  }, [dispatch])

  return (
    <div className="NavLinks">

      <div className="home-logo">
        <Link to="/">
          <span>
            <img style={{ width: 40, height: 40 }} src={logoF} />
            <img src={logoFake} style={{ width: 120, height: 70 }} />
          </span>
        </Link>
      </div>
      <div className="search-field">
        <Search
          className="search"
          placeholder="search products"
          onSearch={onSearch}
          allowClear
          onChange={handleChange}

        />
      </div>
      <div className="links">
        <Link to="/">Home</Link>
        <Link
          to={{
            pathname: '/shop',
            state: { products }
          }}>
          Shop
        </Link>
        <Link to="/about">
          About Us
        </Link>
      </div>
      <div>
        <div className="navlinks-wishlist">
          <span onClick={showModalWishlist}>
            My wish list
          </span>
          <Modal title="My Wish List Items" visible={isModalWishlistVisible} onOk={handleOkWishlist} onCancel={handleCancelWishlist}>
            {user && <Link
              to={{
                pathname: `/user/${user.id}/wishlist`,
                state: { id: user.id }
              }}
              onClick={handleOkWishlist}
            >
              <WishlistModal />
            </Link>}
          </Modal>
        </div>
      </div>

      <div className="visitor">
        <span className="badge-icon" onClick={showModal}>
          <Badge
            count={counter}
            className="badge"
            title='My Cart'
            offset={[-11, 0]} >
            <span className="cart" ><ShoppingCartOutlined /></span>
          </Badge>
        </span>

        <span className="user-login"  >
          {user ? <span onClick={() => dispatch(toggleHidden())}>
            <span>
              <Button className="user" icon={<UserOutlined />} />
              <CaretDownOutlined className="arrrow-down-user" />
            </span>
          </span> : (userT ?
            <span onClick={() => dispatch(toggleHidden())}>
              <span>
                <Button className="user" icon={<UserOutlined />} />
                <CaretDownOutlined className="arrrow-down-user" />
              </span>
            </span>
            : <>
              <Link to='/users/login'><Button type="primary" style={{ marginRight: 10 }}>Login</Button></Link>
              <Link to='/users/signup'><Button type="primary">Signup</Button></Link>
            </>
          )}
        </span>
        {!hidden && <Profile user={user} />}
      </div>
      <Modal
        title="Your Cart Items"
        visible={isModalVisible}
        onCancel={handleCancel}
        style={{ maxHeight: 500, overflowY: "auto" }}
        footer={[
          <Button key="cancel" onClick={handleCancel}>
            Continue shopping
          </Button>,
          <Button key="back" type="primary" onClick={handleOk}>
            Checkout
          </Button>,
        ]}>
        {/* {user && <Cart userId={user.id} />} */}
        {user && <UserCart userId={user.id} />}

      </Modal>
    </div>
  )
}

export default NavLinks
