import axios from 'axios'

const url = "https://fakestoreapi.com"

//Products
export const fetchProducts = () => axios.get(`${url}/products`);

//* Product crud
export const createProduct = (newProduct) => axios.post(`${url}/products`, newProduct);

export const updateProduct = (id, newProduct) => axios.patch(`${url}/products/${id}`, newProduct);
export const deleteProduct = (id) => axios.delete(`${url}/products/${id}`);

export const fetchOneProduct = (id) => axios.get(`${url}/products/${id}`);

//*collections
export const fetchCollection = (category) => axios.get(`${url}/products/category/${category}`);

//carts
export const fetchAllCarts = () => axios.get(`${url}/carts/`)

//cart by user
// export const fetchUserCart = (id) => axios.get(`${url}/carts/user/${id}`)

//*login
export const authLogin = (user) => axios.post("http://192.168.1.24:3000/api/loginUser", user);

//users
export const fetchAllUsers = () => axios.get(`${url}/users/`);

export const createUser = (userData) => axios.post("http://192.168.1.24:3000/api/users", userData)


//user
export const fetchOneUser = (id) => axios.get(`${url}/users/${id}`)