import wishlistTypes from "../constants/wishlistTypes";
import { addItemToWish } from "../actions/wishUtils"

const INITIAL_STATE = {
  createdAt: undefined,
  wishlist: []
}

const wishlist = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case wishlistTypes.ADD_TO_WISHLIST:
      return {
        ...state,
        wishlist: addItemToWish(state.wishlist, action.payload),
        createdAt: (state.createdAt ? state.createdAt : new Date())
      }
    case wishlistTypes.REMOVE_ONE_FROM_WISHLIST:
      return {
        ...state,
        wishlist: state.wishlist.filter(item => item.id !== action.payload.id)
      }
    case wishlistTypes.CLEAR_WISHLIST:
      return {
        ...state,
        wishlist: []
      }
    default:
      return state;
  }
}
export default wishlist