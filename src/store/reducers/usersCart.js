import cartsTypes from "../constants/cartsTypes"

export default (userCart = initialState, action) => {
  switch (action.type) {
    case cartsTypes.FETCH_ALL_CARTS:
      return action.payload;
    default:
      return userCart;
  }
}