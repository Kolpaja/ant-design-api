// eslint-disable-next-line
import Search from "antd/lib/transfer/search";
import productsTypes from "../constants/productsTypes";

const INITIAL_STATE = {
  products: [],
  search: '',
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case productsTypes.FETCH_ALLPRODUCTS:
      return {
        ...state,
        products: action.payload
      };
    case productsTypes.FILTER_PRODUCTS:
      return {
        ...state,
        search: action.payload
      };
    case productsTypes.CREATE_PRODUCT:
      return {
        ...state,
        products: action.payload
      };;
    case productsTypes.UPDATE_PRODUCT:
      return {
        ...state,
        products: action.payload
      };
    case productsTypes.DELETE_PRODUCT:
      return {
        ...state,
        products: state.products.filter(product => product.id !== action.payload)
      };


    default:
      return state
  }
}