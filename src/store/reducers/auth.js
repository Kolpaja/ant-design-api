import usersAuthTypes from "../constants/usersAuthTypes";

const authReducer = (state = { authData: null }, action) => {
  switch (action.type) {
    case usersAuthTypes.AUTH:
      // console.log("reducer auth: ", action?.data)
      localStorage.setItem("token", JSON.stringify(action?.data))
      return { ...state, authData: action?.data }
    case usersAuthTypes.LOGOUT:
      localStorage.clear()
      return state;
    default:
      return state;
  }
};

export default authReducer


//  login res: 
// Object { name: {… }, _id: "6137852a97960e1dd9879f77", email: "fatjona@gmail.com", password: "$2b$10$3pC8Id8UL0GyWL46wVxB2OFTVYqI0gS3EKn29dmkMDNpYWN3RUWiq", username: "anna1", phone: "4355454", __v: 0 }

// __v: 0

// _id: "6137852a97960e1dd9879f77"

// email: "fatjona@gmail.com"

// name: Object { firstname: "anna", lastname: "nana" }

// password: "$2b$10$3pC8Id8UL0GyWL46wVxB2OFTVYqI0gS3EKn29dmkMDNpYWN3RUWiq"

// phone: "4355454"

// username: "anna1"