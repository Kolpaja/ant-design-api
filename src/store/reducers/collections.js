// eslint-disable-next-line
import collectionTypes from "../constants/collectionTypes";


export default (collections = [], action) => {
  switch (action.type) {
    case collectionTypes.FETCH_ALL:
      return action.payload;

    default:
      return collections
  }
}