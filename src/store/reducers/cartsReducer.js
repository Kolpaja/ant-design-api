import cartsTypes from "../constants/cartsTypes";
import { addItemToCart, removeOneItemFromCart } from "../actions/cartUtils"

const INITIAL_STATE = {
  createdAt: new Date(),
  userId: "",
  cartItems: [],
  purchased: false,
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case cartsTypes.ADD_ITEM_TO_CART:
      return {
        ...state,
        cartItems: addItemToCart(state.cartItems, action.payload),
      }
    case cartsTypes.REMOVE_ONE_ITEM_FROM_CART:
      return {
        ...state,
        cartItems: removeOneItemFromCart(state.cartItems, action.payload)
      }
    case cartsTypes.DELETE_ITEM_FROM_CART:
      return {
        ...state,
        cartItems: state.cartItems.filter(
          (cartItem) => cartItem.id !== action.payload.id
        )
      }
    case cartsTypes.ADD_USERID_TO_CART:
      return {
        ...state,
        userId: action.payload
      };
    case cartsTypes.PURCHASE:
      return {
        ...state,
        purchased: true,
      }
    case cartsTypes.CLEAR_CART:
      return {
        cartItems: []
      }
    default:
      return state;
  }
}