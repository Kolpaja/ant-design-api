import usersTypes from "../constants/usersTypes";

const INITIAL_STATE = {
  hidden: true,
  user: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case usersTypes.GET_USER:
      return {
        ...state,
        user: action.payload
      }
    case usersTypes.ADD_USER:
      return {
        ...state,
        user: action.payload
      }
    case usersTypes.LOGOUT_USER:
      return {
        hidden: !state.hidden,
        user: null
      }
    case usersTypes.TOGGLE_INFO:
      return {
        ...state,
        hidden: !state.hidden,
      };
    default:
      return state;
  }
}