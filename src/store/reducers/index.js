import { combineReducers } from "redux";
import products from "./products";
import collections from "./collections"
import auth from './auth';
import users from "./usersReducer";
import user from "./user";
import carts from "./cartsReducer";
import wishlist from "./wishListReducer"



export default combineReducers({
  products,
  collections,
  auth,
  users,
  user,
  carts,
  wishlist,
})
