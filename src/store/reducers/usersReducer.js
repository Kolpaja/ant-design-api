import usersTypes from "../constants/usersTypes";


export default (users = [], action) => {
  switch (action.type) {
    case usersTypes.FETCH_ALL_USERS:
      return action.payload;
    default:
      return users
  }
}