const cartsTypes = {
  FETCH_ALL_CARTS: "FETCH_ALL_CARTS",
  FETCH_ONE_CART: "FETCH_ONE_CART",
  ADD_CART: "ADD_CART",
  DELETE_CART: "DELETE_CART",
  UPDATE_CART: "DELETE_CART",
  GET_USER_CART: "GET_USER_CART",
  ADD_ITEM_TO_CART: "ADD_ITEM_TO_CART",
  REMOVE_ONE_ITEM_FROM_CART: 'REMOVE_ONE_ITEM_FROM_CART',
  DELETE_ITEM_FROM_CART: "DELETE_ITEM_FROM_CART",
  CLEAR_CART: "CLEAR_CART",
  ADD_USERID_TO_CART: "ADD_USERID_TO_CART",
  PURCHASE: "PURCHASE"
}

export default cartsTypes;