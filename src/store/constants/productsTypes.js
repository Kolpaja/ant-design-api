const productsTypes = {
  FETCH_ALLPRODUCTS: "FETCH_ALLPRODUCTS",
  FILTER_PRODUCTS: "FILTER_PRODUCTS",
  CREATE_PRODUCT: "CREATE_PRODUCT",
  UPDATE_PRODUCT: "UPDATE_PRODUCT",
  DELETE_PRODUCT: "DELETE_PRODUCT",
}

export default productsTypes;