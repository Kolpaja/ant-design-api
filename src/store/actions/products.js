import { message } from "antd";
import * as api from "../../api";
import productsTypes from "../constants/productsTypes"

//Action creators

export const getProducts = () => async (dispatch) => {
  try {
    const { data } = await api.fetchProducts();
    // console.log("FETCH_ALL:, ", res)
    dispatch({
      type: productsTypes.FETCH_ALLPRODUCTS, payload: data
    })
  } catch (error) {
    console.log("fetching posts from api error: ", error.message)
  }
};
export const searchProducts = (data) =>
({
  type: productsTypes.FILTER_PRODUCTS, payload: data
})

export const createProduct = (productData) => async (dispatch) => {
  try {
    const res = await api.createProduct(productData);
    // console.log("create product act: ", res)
    dispatch({ type: productsTypes.CREATE_PRODUCT, payload: res?.data });

  } catch (error) {
    console.log(error)
    message.error("No Connection! Try Again Later!")
  }
}

export const updateCurrentProduct = (id, productData) => async (dispatch) => {
  try {
    const res = await api.updateProduct(id, productData);
    // console.log("create product act: ", res)
    dispatch({ type: productsTypes.UPDATE_PRODUCT, payload: res?.data });

  } catch (error) {
    console.log(error)
    message.error("No Connection! Try Again Later!")
  }
}



export const deleteOneProduct = (id) => async (dispatch) => {
  try {
    await api.deleteProduct(id);
    dispatch({ type: productsTypes.DELETE_PRODUCT, payload: id })
  } catch (error) {
    console.log("delete post error: ", error)

  }
}

