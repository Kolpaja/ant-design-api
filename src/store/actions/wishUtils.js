export const addItemToWish = (wishlist, cartItemToAdd) => {
  const existingCartItem = wishlist.find(
    (cartItem) => cartItem.id === cartItemToAdd.id
  );
  if (existingCartItem) {
    return wishlist.map((cartItem) =>
      cartItem.id === cartItemToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }
  return [...wishlist, { ...cartItemToAdd, quantity: 1 }];
};