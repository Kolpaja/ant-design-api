import wishlistTypes from "../constants/wishlistTypes"

export const addToWishlist = (item) => ({
  type: wishlistTypes.ADD_TO_WISHLIST,
  payload: item
})

export const removeOneWish = (item) => ({
  type: wishlistTypes.REMOVE_ONE_FROM_WISHLIST,
  payload: item
})