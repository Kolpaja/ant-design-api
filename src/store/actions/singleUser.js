import * as api from "../../api"
import usersTypes from "../constants/usersTypes";


export const toggleHidden = () => ({
  type: usersTypes.TOGGLE_INFO
})

export const getOneUser = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchOneUser(id);
    // console.log("1 user: ", data)
    dispatch({
      type: usersTypes.GET_USER, payload: data
    })
    // console.log("FETCH_ALL_users:, ", data);
  } catch (error) {
    console.log("fetching user from api error: ", error.message)
  }
};

export const addNewUser = (userData) => async (dispatch) => {
  try {
    const res = await api.createUser(userData);
    console.log("add user action:, ", res.data);
    dispatch({
      type: usersTypes.ADD_USER,
      payload: res.data
    })
  } catch (error) {

    console.log("add user error: ", error)
  }
};

export const logoutUser = () => ({
  type: usersTypes.LOGOUT_USER,
})
