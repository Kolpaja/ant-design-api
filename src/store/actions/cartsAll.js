import * as api from "../../api"
import cartsTypes from "../constants/cartsTypes"


//?all carts
export const getAllCartsItems = () => async (dispatch) => {
  try {
    const { data } = await api.fetchAllCarts();
    console.log("FETCH_ALL carts:, ", data)
    dispatch({
      type: cartsTypes.FETCH_ALL_CARTS, payload: data
    })
  } catch (error) {
    console.log("fetching carts from api error: ", error.message)
  }
};

//? user carts

export const addItem = (item) => ({
  type: cartsTypes.ADD_ITEM_TO_CART,
  payload: item
});

export const removeOneItem = (item) => ({
  type: cartsTypes.REMOVE_ONE_ITEM_FROM_CART,
  payload: item,
});

export const removeItem = (item) => ({
  type: cartsTypes.DELETE_ITEM_FROM_CART,
  payload: item,
})

export const addUserToCart = (userId) => ({
  type: cartsTypes.ADD_USERID_TO_CART,
  payload: userId,
});

export const purchaseCart = (userId) => ({
  type: cartsTypes.PURCHASE,
  payload: userId,
})
// export const removeItem = (item) => ({
//   type: CartActionTypes.REMOVE_ITEM_FROM_CART,
//   payload: item,
// });

export const clearCart = () => ({
  type: cartsTypes.CLEAR_CART,
});