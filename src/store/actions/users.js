import * as api from "../../api";
import usersTypes from "../constants/usersTypes"

//Action creators

export const getAllUsers = () => async (dispatch) => {
  try {
    const { data } = await api.fetchAllUsers();
    dispatch({
      type: usersTypes.FETCH_ALL_USERS, payload: data
    })
    // console.log("FETCH_ALL_users:, ", data);
  } catch (error) {
    console.log("fetching users from api error: ", error.message)
  }
};

