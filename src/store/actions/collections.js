import * as api from "../../api"
import collectionTypes from "../constants/collectionTypes"

//Action creators

export const getCollection = (category) => async (dispatch) => {
  try {
    const { data } = await api.fetchCollection(category);
    // console.log("FETCH_ALL collections: ", data)
    dispatch({
      type: collectionTypes.FETCH_ALL, payload: data
    })
  } catch (error) {
    console.log("fetching collections from api error: ", error.message)
  }
};
