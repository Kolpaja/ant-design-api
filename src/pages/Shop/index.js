import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ProductItem from "../../components/Products/ProductItem"

import { Spin } from "antd"

const ProductsCollection = ({ location }) => {
  const dispatch = useDispatch()
  const search = useSelector(state => state.products.search)
  // console.log("products: ", location)
  const items = location.state.products
  return (
    <>
      {
        items.length > 0 ? items.filter(product => product.title.toLowerCase().includes(search.trim().toLowerCase())).map(
          item => < ProductItem item={item} key={item.id} />
        ) : <Spin />
      }

    </>
  )
}

export default ProductsCollection
