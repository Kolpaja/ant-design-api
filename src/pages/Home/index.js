import React, { useEffect, useState } from 'react';
import "./styles.css"

import Categories from "../../components/Categories"
import Products from '../../components/Products';
import { Divider, Layout, Space } from "antd"
import Users from "../../components/Users"
import { BackTop } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import { getProducts } from "../../store/actions/products"


function Home() {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products.products)
  const userId = useSelector(state => state.user)

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])
  return (
    <Layout className="Home">
      <Categories />
      <Divider>Explore New Incomings</Divider>
      <Products products={products} userId={userId} />
      <Divider>Loyalty Users</Divider>
      <Users />
      <BackTop />
    </Layout>
  )
}

export default Home
