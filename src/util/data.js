// ? Api data backend mngnt

const product = {
  title: 'test product',
  price: 13.5,
  description: 'lorem ipsum set',
  image: 'https://i.pravatar.cc',
  category: 'electronic',
  userId: 1,
  rating: {
    count: 1,
    rate: "nr nga 0 tek 5"
  }
}

const user = {
  name: {
    firstname: 'John',
    lastname: 'Doe'
  },
  email: 'John@gmail.com',
  username: 'johnd',
  password: 'm38rmF$',
  gender: "male",
  city: 'kilcoole',
  country: "albania",
  address: {
    street: '7835 new road',
    number: 3,
  },
  phone: '1-570-236-7033',
  seller: false,
  agreement: true
}

const cart = {
  userId: 5,
  createdAt: 2020 - 02 - 03,
  products: [{ productId: 5, quantity: 1 }, { productId: 1, quantity: 5 }],
  purchased: true
}

const wishlist = {
  userId: 5,
  createdAt: "date",
  wishlist: ["array of products"]
}

//? endpoints

const getProducts = "/products";
const getProduct = "/products/:id";
const CRUDproduct = "/products/:id,(item)";
const getUsersProducts = "/products/user/:id";

const getUsers = "/users"
const getUser = "/users/:id"
const CRUDuser = "/users/:id, user"
//?auth
const auth = "/auth/login"
const login = {
  username: "mor_2314",
  password: "83r5^_"
}

const getCarts = "/carts"
const getCart = "/carts/:id"
const CRUDcart = "/carts/:id, cart"
const getUserCart = "/carts/user/:id"

//? fliter by purched or not to show the logged in user the cart else new cart

//? Puchase history i need only the purchased carts

const getwhilists = "/carts/wishlists"
const getwhishlist = "/carts/wishlists/1"
const CRUDwishlists = "/carts/wishlists/:id, wishlist"
const getUserWishlist = "/carts/wishlist/user/:id"


import stripe from "stripe";
const stripeApi = stripe(process.env.STRIPE_SECRET_KEY);

app.post("/payment", (req, res) => {
  const body = {
    source: req.body.token.id,
    amount: req.body.amount,
    currency: "usd",
  };
  stripeApi.charges.create(body, (stripeErr, stripeRes) => {
    if (stripeErr) {
      res.status(500).send({ error: stripeErr });
    } else {
      res.status(200).send({ success: stripeRes });
    }
  });
});